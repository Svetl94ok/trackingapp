﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Models
{
    [Table("Locations")]
    public class Location
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Location Name is required field")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Address is required field")]
        public string Address { get; set; }

    }
}
