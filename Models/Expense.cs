﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models
{
    [Table("Expenses")]
    public class Expense
    {
            public int Id { get; set; }

            [Required(ErrorMessage = "Details is required field")]
            public string Details { get; set; }

            [Required(ErrorMessage = "Amount is required field"), RegularExpression(@"\d+([,\.]\d+)?", ErrorMessage = "Decimal type is only allowed")]
            public decimal Amount { get; set; }
       
    }
}
