﻿using System.ComponentModel.DataAnnotations;


namespace Models
{
    public class User
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "e-mail is required"), DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required(ErrorMessage = "Password is required"), DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
