﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models
{
    [Table("Meetings")]

    public class Meeting
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required field")]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required(ErrorMessage = "Meeting Date is required field")]
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "Location is required field")]
        public int Location_ID { get; set; }
        public Location Location { get; set; }

    }
}

