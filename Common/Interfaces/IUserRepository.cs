﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Interfaces
{
    public interface IUserRepository
    {
        User GetUserById(int? id);
        IEnumerable<User> GetUsers();
        void InsertNew(User user);
        void Update(User user, int? id);
        void Delete(int? id);
    }
}
