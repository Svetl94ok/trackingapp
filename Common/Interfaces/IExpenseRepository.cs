﻿using Models;
using System.Collections.Generic;

namespace Common.Interfaces
{
    public interface IExpenseRepository
    {
        IEnumerable<Expense> GetExpenses();
        Expense GetExpenseById(int? id);
        void InsertNew(Expense expense);
        void Update(Expense expense, int? id);
        void Delete(int? id);
        void ExpensesUpdate(List<Expense> expenses);
    }
}
