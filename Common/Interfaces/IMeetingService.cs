﻿using Models;
using System.Collections.Generic;

namespace Common.Interfaces
{
    public interface IMeetingService
    {
        public void Delete(int? id);

        public Meeting GetMeetingById(int? id);

        public IEnumerable<Meeting> GetMeetings();
        public void InsertNew(Meeting meeting);
        public void Update(Meeting meeting, int? id);
    }
}
