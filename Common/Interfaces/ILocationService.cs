﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Interfaces
{
    public interface  ILocationService
    {
        public void Delete(int? id);

        public Location GetLocationById(int? id);

        public IEnumerable<Location> GetLocations();
        public void InsertNew(Location location);
        public void Update(Location location, int? id);
    }
}
