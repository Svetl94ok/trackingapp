﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Interfaces
{
    public interface IUserService
    {
        public void Delete(int? id);

        public User GetUsergById(int? id);

        public IEnumerable<User> GetUsers();
        public void InsertNew(User user);
        public void Update(User user, int? id);

    }
}
