﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Interfaces
{
    public interface IMeetingRepository
    {
        IEnumerable<Meeting> GetMeetings();
        Meeting GetMeetingById(int? id);
        void InsertNew(Meeting meeting);
        void Update(Meeting meeting, int? id);
        void Delete(int? id);
    }
}
