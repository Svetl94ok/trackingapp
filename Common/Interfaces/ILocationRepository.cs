﻿using Models;
using System.Collections.Generic;

namespace Common.Interfaces
{
    public interface ILocationRepository
    {
        IEnumerable<Location> GetLocations();
        Location GetLocationById(int? id);
        void InsertNew(Location location);
        void Update(Location location, int? id);
        void Delete(int? id);


    }
}
