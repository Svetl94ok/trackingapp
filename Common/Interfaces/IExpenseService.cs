﻿using Models;
using System.Collections.Generic;

namespace Common.Interfaces
{
    public interface IExpenseService
    {
        public void Delete(int? id);

        public Expense GetExpenseById(int? id);

        public IEnumerable<Expense> GetExpenses();
        public void InsertNew(Expense expense);
        public void Update(Expense expense, int? id);

        public void ExpensesUpdate(List<Expense> expenses);
    }
}
