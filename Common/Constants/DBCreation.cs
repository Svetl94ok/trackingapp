﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace Common.Constants
{
    public class DBCreation
    {
        public static bool CheckDatabaseExists(string connectionString, string databaseName)
        {
            using (var connection = new SqlConnection(DBConst.initConnection))
            {
                using (var command = new SqlCommand($"SELECT db_id('{DBConst.databaseName}')", connection))
                {
                    connection.Open();
                    return (command.ExecuteScalar() != DBNull.Value);
                }
            }
        }

        public static void CreateDataBase()
        {
            bool dbExist = CheckDatabaseExists(DBConst.initConnection, DBConst.databaseName);
            if (!dbExist)
            {
                SqlConnection con = new SqlConnection(DBConst.initConnection);

                string dbCreationquery = DBCreationStoredProcedures.DBCreationQuery();
                SqlCommand dbCreationcmd = new SqlCommand(dbCreationquery, con);

                string usersTablesCreation = DBCreationStoredProcedures.UsersTablesCreation();
                SqlCommand usersTablesCreationcmd = new SqlCommand(usersTablesCreation, con);
                string locationsTableCreation = DBCreationStoredProcedures.LocationsTablesCreation();
                SqlCommand locationsTableCreationcmd = new SqlCommand(locationsTableCreation, con);
                string expensesTableCreation = DBCreationStoredProcedures.ExpensesTablesCreation();
                SqlCommand expensesTableCreationcmd = new SqlCommand(expensesTableCreation, con);
                string meetingsTableCreation = DBCreationStoredProcedures.MeetingsTablesCreation();
                SqlCommand meetingsTableCreationcmd = new SqlCommand(meetingsTableCreation, con);
                string udtExpenseTableCreation = DBCreationStoredProcedures.UDTExpenseTableCreation();
                SqlCommand udtExpenseTableCreationcmd = new SqlCommand(udtExpenseTableCreation, con);

                try
                {
                    con.Open();
                    dbCreationcmd.ExecuteNonQuery();
                    usersTablesCreationcmd.ExecuteNonQuery();
                    locationsTableCreationcmd.ExecuteNonQuery();
                    expensesTableCreationcmd.ExecuteNonQuery();
                    meetingsTableCreationcmd.ExecuteNonQuery();
                    udtExpenseTableCreationcmd.ExecuteNonQuery();


                }

                catch (SqlException e)
                {
                    Console.WriteLine("Error Generated. Details: " + e.ToString());
                }
                finally
                {
                    con.Close();
                }


                SqlConnection connToDB = new SqlConnection(DBConst.connectionString);

                string getAllTablesSP = DBCreationStoredProcedures.GetAllTablesSP();
                SqlCommand getAllTablescmd = new SqlCommand(getAllTablesSP, connToDB);

                string locationAddNewSP = DBCreationStoredProcedures.LocationAddNewSP();
                SqlCommand locationAddNewcmd = new SqlCommand(locationAddNewSP, connToDB);
                string locationUpdateSP = DBCreationStoredProcedures.LocationUpdateSP();
                SqlCommand locationUpdatecmd = new SqlCommand(locationUpdateSP, connToDB);
                string locationDeleteSP = DBCreationStoredProcedures.LocationDeleteSP();
                SqlCommand locationDeletecmd = new SqlCommand(locationDeleteSP, connToDB);
                string locationGetSP = DBCreationStoredProcedures.LocationGetSP();
                SqlCommand locationGetcmd = new SqlCommand(locationGetSP, connToDB);
                string locationGetByIdSP = DBCreationStoredProcedures.LocationGetByIdSP();
                SqlCommand locationGetByIdcmd = new SqlCommand(locationGetByIdSP, connToDB);

                string expensesAddNewSP = DBCreationStoredProcedures.ExpnseAddNewSP();
                SqlCommand expensesAddNewcmd = new SqlCommand(expensesAddNewSP, connToDB);
                string expensesUpdateSP = DBCreationStoredProcedures.ExpnseUpdateSP();
                SqlCommand expensesUpdatecmd = new SqlCommand(expensesUpdateSP, connToDB);
                string expensesDeleteSP = DBCreationStoredProcedures.ExpnseDeleteSP();
                SqlCommand expensesDeletecmd = new SqlCommand(expensesDeleteSP, connToDB);
                string expensesGetSP = DBCreationStoredProcedures.ExpnseGetSP();
                SqlCommand expensesGetcmd = new SqlCommand(expensesGetSP, connToDB);
                string expensesGetByIdSP = DBCreationStoredProcedures.ExpnseGetByIdSP();
                SqlCommand expensesGetByIdcmd = new SqlCommand(expensesGetByIdSP, connToDB);
                string expensesListUpdateSP = DBCreationStoredProcedures.ExpensesListUpdateSP();
                SqlCommand expensesListUpdateSPcmd = new SqlCommand(expensesListUpdateSP, connToDB);
                string expensesListUpdateJSONSP = DBCreationStoredProcedures.ExpensesListUpdateJSONSP();
                SqlCommand expensesListUpdateJSONSPcmd = new SqlCommand(expensesListUpdateJSONSP, connToDB);

                string meetingAddNewSP = DBCreationStoredProcedures.MeetingAddNewSP();
                SqlCommand meetingAddNewcmd = new SqlCommand(meetingAddNewSP, connToDB);
                string meetingUpdateSP = DBCreationStoredProcedures.MeetingUpdateSP();
                SqlCommand meetingUpdatecmd = new SqlCommand(meetingUpdateSP, connToDB);
                string meetingDeleteSP = DBCreationStoredProcedures.MeetingDeleteSP();
                SqlCommand meetingDeletecmd = new SqlCommand(meetingDeleteSP, connToDB);
                string meetingGetSP = DBCreationStoredProcedures.MeetingGetSP();
                SqlCommand meetingGetcmd = new SqlCommand(meetingGetSP, connToDB);
                string meetingGetByIdSP = DBCreationStoredProcedures.MeetingGetByIdSP();
                SqlCommand meetingGetByIdcmd = new SqlCommand(meetingGetByIdSP, connToDB);

                string userAddNewSP = DBCreationStoredProcedures.UserAddNewSP();
                SqlCommand userAddNewcmd = new SqlCommand(userAddNewSP, connToDB);
                string userUpdateSP = DBCreationStoredProcedures.UserUpdateSP();
                SqlCommand userUpdatecmd = new SqlCommand(userUpdateSP, connToDB);
                string userDeleteSP = DBCreationStoredProcedures.UserDeleteSP();
                SqlCommand userDeletecmd = new SqlCommand(userDeleteSP, connToDB);
                string userGetSP = DBCreationStoredProcedures.UserGetSP();
                SqlCommand userGetcmd = new SqlCommand(userGetSP, connToDB);
                string userGetByIdSP = DBCreationStoredProcedures.UserGetByIdSP();
                SqlCommand userGetByIdcmd = new SqlCommand(userGetByIdSP, connToDB);

                string locationInitialDataSP = DBCreationStoredProcedures.LocationInitialDataSP();
                SqlCommand locationInitialDatacmd = new SqlCommand(locationInitialDataSP, connToDB);
                string expenseInitialDataSP = DBCreationStoredProcedures.ExpenseInitialDataSP();
                SqlCommand expenseInitialDatacmd = new SqlCommand(expenseInitialDataSP, connToDB);
                string meetingsInitialDataSP = DBCreationStoredProcedures.MeetingInitialDataSP();
                SqlCommand meetingsInitialDatacmd = new SqlCommand(meetingsInitialDataSP, connToDB);
                string userInitialDataSP = DBCreationStoredProcedures.UserInitialDataSP();
                SqlCommand userInitialDatacmd = new SqlCommand(userInitialDataSP, connToDB);

                try {

                    connToDB.Open();
                    getAllTablescmd.ExecuteNonQuery();

                    locationAddNewcmd.ExecuteNonQuery();
                    locationUpdatecmd.ExecuteNonQuery();
                    locationDeletecmd.ExecuteNonQuery();
                    locationGetcmd.ExecuteNonQuery();
                    locationGetByIdcmd.ExecuteNonQuery();

                    expensesAddNewcmd.ExecuteNonQuery();
                    expensesUpdatecmd.ExecuteNonQuery();
                    expensesDeletecmd.ExecuteNonQuery();
                    expensesGetcmd.ExecuteNonQuery();
                    expensesGetByIdcmd.ExecuteNonQuery();
                    expensesListUpdateSPcmd.ExecuteNonQuery();
                    expensesListUpdateJSONSPcmd.ExecuteNonQuery();

                    meetingAddNewcmd.ExecuteNonQuery();
                    meetingUpdatecmd.ExecuteNonQuery();
                    meetingDeletecmd.ExecuteNonQuery();
                    meetingGetcmd.ExecuteNonQuery();
                    meetingGetByIdcmd.ExecuteNonQuery();

                    userAddNewcmd.ExecuteNonQuery();
                    userUpdatecmd.ExecuteNonQuery();
                    userDeletecmd.ExecuteNonQuery();
                    userGetcmd.ExecuteNonQuery();
                    userGetByIdcmd.ExecuteNonQuery();

                    locationInitialDatacmd.ExecuteNonQuery();
                    expenseInitialDatacmd.ExecuteNonQuery();
                    meetingsInitialDatacmd.ExecuteNonQuery();
                    userInitialDatacmd.ExecuteNonQuery();

                }

                catch (SqlException e)
                {
                    Console.WriteLine("Error Generated. Details: " + e.ToString());
                }
                finally
                {
                    connToDB.Close();
                }
            }


                SqlConnection connection = new SqlConnection(DBConst.connectionString);

            try { 

                connection.Open();

                string location_InitialData = StoredProcedures.location_InitialData;
                SqlCommand location_InitialDatacmd = new SqlCommand(location_InitialData, connection);
                string expenses_InitialData = StoredProcedures.expenses_InitialData;
                SqlCommand expenses_InitialDatacmd = new SqlCommand(expenses_InitialData, connection);
                string meetings_InitialData = StoredProcedures.meeting_InitialData;
                SqlCommand meetings_InitialDatacmd = new SqlCommand(meetings_InitialData, connection);
                string user_InitialData = StoredProcedures.user_InitialData;
                SqlCommand user_InitialDatacmd = new SqlCommand(user_InitialData, connection);


                var adapter = new SqlDataAdapter(StoredProcedures.getAllTables, connection);
                var ds = new DataSet();
                adapter.Fill(ds);

                if (ds.Tables["Table"].Rows.Count == 0)
                {
                    location_InitialDatacmd.ExecuteNonQuery();
                }
                if (ds.Tables["Table1"].Rows.Count == 0)
                {

                    expenses_InitialDatacmd.ExecuteNonQuery();
                }
                if (ds.Tables["Table2"].Rows.Count == 0)
                {
                    meetings_InitialDatacmd.ExecuteNonQuery();
                }
                if (ds.Tables["Table3"].Rows.Count == 0)
                {
                    user_InitialDatacmd.ExecuteNonQuery();
                }

            }
            catch (SqlException e)
            {
                Console.WriteLine("Error Generated. Details: " + e.ToString());
            }
            finally
            {
                connection.Close();
            }
        }
    }
}


