﻿

namespace Common.Constants
{
    public class DBCreationStoredProcedures
    {
        public static string DBCreationQuery()
        {
            string dataBaseAndTablesCreation = $"Create database {DBConst.databaseName}";
            return dataBaseAndTablesCreation;
        }

        public static string UDTExpenseTableCreation()
        {
            string udtExpenseTableCreation = "Create TYPE udtExpenses AS Table (ExpenseID INT, ExpenseDetails nvarchar(max),ExpenseAmount decimal(10,2) ) GO";
            return udtExpenseTableCreation;
        }

        public static string UsersTablesCreation()
        {
            string usersTablesCreation = "USE TrackingApplication Create table Users (ID int IDENTITY(1, 1) NOT NULL, Email[nvarchar](50) NOT NULL, Password[nvarchar](50) NOT NULL, PRIMARY KEY (ID))";
            return usersTablesCreation;
        }
        public static string LocationsTablesCreation()
        {
            string locationsTablesCreation = "USE TrackingApplication Create table Locations (ID int IDENTITY(1, 1) NOT NULL, Name[nvarchar](50) NOT NULL, Address[nvarchar](100) NOT NULL, PRIMARY KEY (ID))";
            return locationsTablesCreation;
        }
        public static string ExpensesTablesCreation()
        {
            string expensesTablesCreation = "USE TrackingApplication Create table Expenses (ID int IDENTITY(1, 1) NOT NULL, Details[nvarchar](MAX) NOT NULL, Amount[decimal](10, 2) NOT NULL, PRIMARY KEY (ID))";
            return expensesTablesCreation;
        }
        public static string MeetingsTablesCreation()
        {
            string meetingsTablesCreation = "USE TrackingApplication Create table Meetings (ID int IDENTITY(1, 1) NOT NULL PRIMARY KEY (ID), Name[nvarchar](50) NOT NULL, Date[date], Description [nvarchar](MAX), Location_ID int, FOREIGN KEY(Location_ID) REFERENCES Locations(ID))";
            return meetingsTablesCreation;
        }
        public static string DBCreationSP()
        {
            string dataBaseAndTablesCreation = "USE TrackingApplication create procedure DataBaseAndTablesCreation AS begin Create database TrackingApp Create table Locations (ID int IDENTITY(1, 1) NOT NULL, Name[nvarchar](50) NOT NULL, Address[nvarchar](100) NOT NULL) Create table Expenses (ID int IDENTITY(1, 1) NOT NULL, Details[nvarchar](MAX) NOT NULL, Amount[decimal](10, 2) NOT NULL) Create table Meetings (ID int IDENTITY(1, 1) NOT NULL, Name[nvarchar](50) NOT NULL, Date[date], FOREIGN KEY(Location_ID) REFERENCES Locations(ID)) end";
            return dataBaseAndTablesCreation;
        }

        public static string GetAllTablesSP()
        {
            string getAllTablesSP = "create procedure GetAllTables as begin select * from Locations select * from Expenses select * from Meetings select * from Users end";
            return getAllTablesSP;
        }

        public static string UserInitialDataSP()
        {
            string userInitialData = "create procedure User_InitialData as begin insert into Users(Email, Password) values('Admin', 'Password123!') end";
            return userInitialData;
        }
        public static string LocationInitialDataSP()
        {
            string locationInitialData = "create procedure Location_InitialData as begin insert into Locations(Name, Address) values('Parallel', 'Kharkiv, Mironositskaya street'), ('Brama', 'Kharkiv, Karazina street') end";
            return locationInitialData;
        }
        public static string ExpenseInitialDataSP()
        {
            string expenseInitialData = "create procedure Expenses_InitialData as begin insert into Expenses(Details, Amount) values('lunch', 100.50), ('dinner', 255.77) end";
            return expenseInitialData;
        }
        public static string MeetingInitialDataSP()
        {
            string meetingInitialData = "create procedure Meeting_InitialData as begin insert into Meetings(Name, Description, Date, Location_ID) values('Meeting 1st', 'internal meeting', '2001-12-09', 1), ('Meeting 2nd', 'internal meeting', '2001-10-10', 2) end";
            return meetingInitialData;
        }

        public static string ExpnseAddNewSP()
        {
            string expenseAddNewSP = "create procedure Expenses_AddNew ( @Details nvarchar(MAX), @Amount decimal(10, 2)) as begin Insert into Expenses(Details, Amount) values(@Details, @Amount) End";
            return expenseAddNewSP;
        }

        public static string ExpensesListUpdateJSONSP()
        {
            string expensesListUpdateJSON = "USE TrackingApplication create procedure Expenses_ListUpdateJSON @MainJson nvarchar (max) = null as begin if (ISJSON(@MainJson) <> 1) begin RAISERROR('@MainJson has not valid format', 16,1) RETURN; end Create Table #Expenses (Id int, Details nvarchar(max), Amount decimal (10,2)) DECLARE @tempJSON TABLE(expense nvarchar(MAX)); INSERT INTO  @tempJSON VALUES(@MainJson) INSERT #Expenses SELECT Id, Details, Amount FROM @tempJSON f CROSS APPLY OPENJSON(expense) WITH(Id int, Details nvarchar(max), Amount decimal (10,2)) UPDATE e SET e.Details = tExp.Details, e.Amount = tExp.Amount FROM #Expenses tExp JOIN Expenses e ON e.ID = tExp.Id end";
            return expensesListUpdateJSON;
        }
        public static string ExpnseUpdateSP()
        {
            string expenseUpdateSP = "create procedure Expenses_Update ( @ID INTEGER, @Details nvarchar(MAX), @Amount decimal(10, 2)) as begin Update Expenses set Details = @Details, Amount = @Amount where ID = @ID End";
            return expenseUpdateSP;
        }
        public static string ExpnseDeleteSP()
        {
            string expenseDeleteSP = "create procedure Expenses_Delete (@ID int) as begin Delete from Expenses where ID = @ID End";
            return expenseDeleteSP;
        }
        public static string ExpnseGetSP()
        {
            string expenseGetSP = "create procedure Expenses_Get as Begin select* from Expenses End";
            return expenseGetSP;
        }

        public static string ExpnseGetByIdSP()
        {
            string expenseGetByIdSP = "create procedure Expenses_GetById (@Id int) as begin select * from[dbo].Expenses where ID = @Id end";
            return expenseGetByIdSP;
        }

        public static string ExpensesListUpdateSP()
        {
            string expensesListUpdateSP = "create procedure Expenses_ListUpdate @TableParam dbo.udtExpenses readonly as begin UPDATE e SET e.Details = udt.ExpenseDetails, e.Amount = udt.ExpenseAmount FROM @TableParam udt JOIN Expenses e ON e.ID = udt.ExpenseID End";
            return expensesListUpdateSP;
        }
        public static string LocationAddNewSP()
        {
            string locationAddNewSP = "create procedure Location_AddNew (@Name nvarchar(50), @Address nvarchar(100)) as begin insert into Locations(Name, Address) values(@Name, @Address) end";
            return locationAddNewSP;
        }

        public static string LocationUpdateSP()
        {
            string locationUpdateSP = "create procedure Location_Update (@Id int, @Name nvarchar(50), @Address nvarchar(100)) as begin update Locations set Name = @Name, Address = @Address where Id = @Id end";
            return locationUpdateSP;
        }
        public static string LocationDeleteSP()
        {
            string locationDeleteSP = "create procedure Location_Delete (@Id int) as begin delete from Locations where Id = @Id end";
            return locationDeleteSP;
        }
        public static string LocationGetSP()
        {
            string locationGetSP = "create procedure Location_Get as begin select * from[dbo].[Locations] end";
            return locationGetSP;
        }

        public static string LocationGetByIdSP()
        {
            string locationGetByIdSP = "create procedure Location_GetById (@Id int) as begin select* from [dbo].[Locations] where ID = @Id end";
            return locationGetByIdSP;
        }

        public static string MeetingAddNewSP()
        {
            string meetingAddNewSP = "create procedure Meeting_AddNew (@Name nvarchar(50), @Description nvarchar(MAX), @Date date, @Location_ID int) as begin insert into Meetings(Name, Description, Date, Location_ID) values(@Name, @Description, @Date, @Location_ID) end";
            return meetingAddNewSP;
        }

        public static string MeetingUpdateSP()
        {
            string meetingUpdateSP = "create procedure Meeting_Update (@Id int, @Name nvarchar(50), @Description nvarchar(MAX), @Date date, @Location_ID int) as begin Update Meetings set Name = @Name, Description = @Description, Date = @Date, Location_ID = @Location_ID where ID = @Id end ";
            return meetingUpdateSP;
        }
        public static string MeetingDeleteSP()
        {
            string meetingDeleteSP = "create procedure Meeting_Delete (@Id int) as begin delete from Meetings where Id = @Id end";
            return meetingDeleteSP;
        }
        public static string MeetingGetSP()
        {
            string meetingGetSP = "create procedure Meeting_Get as Begin select * from Meetings End";
            return meetingGetSP;
        }

        public static string MeetingGetByIdSP()
        {
            string meetingGetByIdSP = "create procedure Meeting_GetById (@Id int) as Begin select * from Meetings where @Id = ID End";
            return meetingGetByIdSP;
        }

        public static string UserAddNewSP()
        {
            string userAddNewSP = "create procedure User_AddNew ( @Email nvarchar(50), @Password nvarchar(50)) as begin Insert into Users(Email, Password) values(@Email, @Password) End";
            return userAddNewSP;
        }

        public static string UserUpdateSP()
        {
            string userUpdateSP = "create procedure User_Update ( @ID INTEGER, @Email nvarchar(50), @Password nvarchar(50)) as begin Update Users set Email = @Password where ID = @ID End";
            return userUpdateSP;
        }
        public static string UserDeleteSP()
        {
            string userDeleteSP = "create procedure User_Delete (@ID int) as begin Delete from Users where ID = @ID End";
            return userDeleteSP;
        }

        public static string UserGetByIdSP()
        {
            string userGetByIdSP = "create procedure User_GetById (@Id int) as begin select * from Users where ID = @Id end";
            return userGetByIdSP;
        }

        public static string UserGetSP()
        {
            string userGetSP = "create procedure User_Get as begin select * from Users end";
            return userGetSP;
        }

    }
}

