﻿

namespace Common.Constants
{
    public static class StoredProcedures
    {
        public const string dataBaseAndTablesCreation = "dbo.DataBaseAndTablesCreation";
        public const string getAllTables = "dbo.GetAllTables";


        public const string location_AddNew = "dbo.Location_AddNew";
        public const string location_Delete = "dbo.Location_Delete";
        public const string location_Get = "dbo.Location_Get";
        public const string location_Update = "dbo.Location_Update";
        public const string location_GetById = "dbo.Location_GetById";
        public const string location_InitialData = "dbo.Location_InitialData";

        public const string expenses_AddNew = "dbo.Expenses_AddNew";
        public const string expenses_Delete = "dbo.Expenses_Delete";
        public const string expenses_Get = "dbo.Expenses_Get";
        public const string expenses_Update = "dbo.Expenses_Update";
        public const string expenses_GetById = "dbo.Expenses_GetById";
        public const string expenses_InitialData = "dbo.Expenses_InitialData";
        public const string expenses_ListUpdate = "dbo.Expenses_ListUpdate";
        public const string expenses_ListUpdateJSON = "dbo.Expenses_ListUpdateJSON";

        public const string meeting_AddNew = "dbo.Meeting_AddNew";
        public const string meeting_Delete = "dbo.Meeting_Delete";
        public const string meeting_Get = "dbo.Meeting_Get";
        public const string meeting_GetAll_JOIN_Location = "dbo.Meeting_GetAll_JOIN_Location";
        public const string meeting_Update = "dbo.Meeting_Update";
        public const string meeting_GetById = "dbo.Meeting_GetById";
        public const string meeting_GetById_JOIN_Location = "dbo.Meeting_GetById_JOIN_Location";
        public const string meeting_InitialData = "dbo.Meeting_InitialData";

        public const string user_AddNew = "dbo.User_AddNew";
        public const string user_Delete = "dbo.User_Delete";
        public const string user_Get = "dbo.User_Get";
        public const string user_Update = "dbo.User_Update";
        public const string user_GetById = "dbo.User_GetById";
        public const string user_InitialData = "dbo.User_InitialData";
    }
}
