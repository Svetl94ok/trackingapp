﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DAL
{
    public class BaseRepository
    {
        protected readonly string _connectionString;

        protected BaseRepository (string connectionString)
        {
            _connectionString = connectionString;
        }

        protected DataTable ExecuteDataTable(
          CommandType commandType,
          string query,
          SqlParameter[] dictParameters = null)
        {
            var dataTable = new DataTable();
            var connection = GetConnection();

            try
            {
                using (var sqlCommand = GetSqlCommandObject(commandType, query, dictParameters, connection))
                {
                    using (var sqlDataAdapter = new SqlDataAdapter(sqlCommand))
                    {
                        sqlDataAdapter.Fill(dataTable);
                    }
                }
            }
            finally
            {
                CloseConnection(connection);
            }

            return dataTable;
        }

        protected DataSet ExecuteDataSet(
            CommandType commandType,
            string query,
            SqlParameter[] dictParameters = null)
        {
            var dataSet = new DataSet();
            var connection = GetConnection();

            try
            {
                using (var sqlCommand = GetSqlCommandObject(commandType, query, dictParameters, connection))
                {
                    using (var sqlDataAdapter = new SqlDataAdapter(sqlCommand))
                    {
                        sqlDataAdapter.Fill(dataSet);
                    }
                }
            }
            finally
            {
                CloseConnection(connection);
            }

            return dataSet;
        }


        protected void ExecuteNonQuery(
            CommandType commandType,
            string query,
            SqlParameter[] dictParameters = null)
        {
            var connection = GetConnection();

            try
            {
                using (var sqlCommand = GetSqlCommandObject(commandType, query, dictParameters, connection))
                {
                    sqlCommand.ExecuteNonQuery();
                }
            }
            finally
            {
                CloseConnection(connection);
            }
        }

            public List<T> ConvertDataTableToList<T>(DataTable dt)
            {
                var columnNames = dt.Columns.Cast <DataColumn>().Select(c => c.ColumnName.ToLower()).ToList();
                var properties = typeof(T).GetProperties();
                return dt.AsEnumerable().Select(row => {
                    var objT = Activator.CreateInstance<T>();
                    foreach (var pro in properties)
                    {
                        if (columnNames.Contains(pro.Name.ToLower()))
                        {
                            try
                            {
                                pro.SetValue(objT, Convert.ChangeType(row[pro.Name], pro.PropertyType));
                            }
                            catch { }
                        }
                    }
                    return objT;
                }).ToList();
            }
        

        private SqlCommand GetSqlCommandObject(CommandType commandType, string query, SqlParameter[] parameters, SqlConnection connection)
        {
            var sqlCommand = new SqlCommand(query, connection)
            {
                CommandType = commandType,
            };

            if (parameters == null)
            {
                return sqlCommand;
            }

            sqlCommand.Parameters.AddRange(parameters);

            return sqlCommand;
        }
       
        protected SqlConnection GetConnection()
        {
            var connection = new SqlConnection(_connectionString);
            connection.Open();

            return connection;
        }

        protected void CloseConnection(SqlConnection connection)
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            connection.Dispose();
        }
    }
}
