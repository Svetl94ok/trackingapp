﻿using Common.Constants;
using Common.Interfaces;
using Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DAL
{
    public class MeetingRepository : BaseRepository, IMeetingRepository
    {
        public MeetingRepository(string connectionString) : base(connectionString)
        {
        }

        public void Delete(int? Id)
        {
            SqlParameter[] parameters = { new SqlParameter("@Id", SqlDbType.Int) { Value = Id } };
            base.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedures.meeting_Delete, parameters);
        }

        public Meeting GetMeetingById(int? Id)
        {
            SqlParameter[] parameters = { new SqlParameter("@Id", SqlDbType.Int) { Value = Id } };

            return base.ConvertDataTableToList<Meeting>(base.ExecuteDataTable(CommandType.StoredProcedure, StoredProcedures.meeting_GetById, parameters)).FirstOrDefault();
        }

        public IEnumerable<Meeting> GetMeetings()
        {
            return base.ConvertDataTableToList<Meeting>(base.ExecuteDataTable(CommandType.StoredProcedure, StoredProcedures.meeting_Get));
        }

        public void InsertNew(Meeting meeting)
        {
            SqlParameter[] parameters = {
                                          new SqlParameter("@Name", SqlDbType.NVarChar){Value = meeting.Name},
                                          new SqlParameter("@Description", SqlDbType.NVarChar) {Value = meeting.Description},
                                          new SqlParameter("@Date", SqlDbType.Date) {Value = meeting.Date},
                                          new SqlParameter("@Location_ID", SqlDbType.Int){Value = meeting.Location_ID}
                                          
            };
            base.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedures.meeting_AddNew, parameters);
        }

        public void Update(Meeting meeting, int? Id)
        {
            SqlParameter[] parameters = {
                                          new SqlParameter("@Id", SqlDbType.Int) { Value = Id } ,
                                          new SqlParameter("@Name", SqlDbType.NVarChar){Value = meeting.Name},
                                          new SqlParameter("@Description", SqlDbType.NVarChar) {Value = meeting.Description},
                                          new SqlParameter("@Date", SqlDbType.Date) {Value = meeting.Date},
                                          new SqlParameter("@Location_ID", SqlDbType.NVarChar){Value = meeting.Location_ID}
            };

            base.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedures.meeting_Update, parameters);
        }
    }
}
