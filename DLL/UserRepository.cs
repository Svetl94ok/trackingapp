﻿using Common.Constants;
using Common.Interfaces;
using Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DAL
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public UserRepository(string connectionString) : base(connectionString)
        {
        }

        void IUserRepository.Delete(int? id)
        {
            SqlParameter[] parameters = { new SqlParameter("@Id", SqlDbType.Int) { Value = id } };
            base.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedures.user_Delete, parameters);
        }

        User IUserRepository.GetUserById(int? id)
        {
            SqlParameter[] parameters = { new SqlParameter("@Id", SqlDbType.Int) { Value = id } };

            return base.ConvertDataTableToList<User>(base.ExecuteDataTable(CommandType.StoredProcedure, StoredProcedures.user_GetById, parameters)).FirstOrDefault();
        }

        void IUserRepository.InsertNew(User user)
        {

            SqlParameter[] parameters = {
                                         new SqlParameter("@Email", SqlDbType.NVarChar){Value = user.Email},
                                          new SqlParameter("@Password", SqlDbType.Decimal) {Value = user.Password}
            };
            base.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedures.user_AddNew, parameters);
        }

        void IUserRepository.Update(User user, int? id)
        {

            SqlParameter[] parameters = { new SqlParameter("@Id", SqlDbType.Int) {Value = id} ,
                                          new SqlParameter("@Email", SqlDbType.NVarChar) {Value = user.Email},
                                          new SqlParameter("@Password", SqlDbType.Decimal) {Value = user.Password}
            };

            base.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedures.user_Update, parameters);
        }
        public IEnumerable<User> GetUsers()
        {
            return base.ConvertDataTableToList<User>(base.ExecuteDataTable(CommandType.StoredProcedure, StoredProcedures.user_Get));
        }
    }
}
