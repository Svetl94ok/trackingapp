﻿using Common.Constants;
using Common.Interfaces;
using Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DAL.Repositories
{
    public class LocationRepository : BaseRepository, ILocationRepository
    {
        public LocationRepository(string connectionString) : base(connectionString)
        {
        }

        public void Delete(int? Id)
        {           
            SqlParameter[] parameters = { new SqlParameter("@Id", SqlDbType.Int) { Value = Id } };
            base.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedures.location_Delete, parameters);
        }

        public Location GetLocationById(int? Id)
        {
         
            SqlParameter[] parameters = { new SqlParameter("@Id", SqlDbType.Int) { Value = Id } };

            return base.ConvertDataTableToList<Location>(base.ExecuteDataTable(CommandType.StoredProcedure, StoredProcedures.location_GetById, parameters)).FirstOrDefault();
        }

        public IEnumerable<Location> GetLocations()
        {
            return base.ConvertDataTableToList<Location>(base.ExecuteDataTable(CommandType.StoredProcedure, StoredProcedures.location_Get));
        }

        public void InsertNew(Location location)
        {
            SqlParameter[] parameters = { 
                                         new SqlParameter("@Name", SqlDbType.NVarChar){Value = location.Name},
                                          new SqlParameter("@Address", SqlDbType.NVarChar) {Value = location.Address}
            };
            base.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedures.location_AddNew, parameters);
        }

        public void Update(Location location, int? Id)
        {

            SqlParameter[] parameters = { new SqlParameter("@Id", SqlDbType.Int) { Value = Id } ,
                                          new SqlParameter("@Name", SqlDbType.NVarChar) { Value = location.Name },
                                          new SqlParameter("@Address", SqlDbType.NVarChar) { Value = location.Address }
            };

            base.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedures.location_Update, parameters);
        }
    }

}
