﻿using Common.Constants;
using Common.Interfaces;
using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.Json;

namespace DAL
{
    public class ExpenseRepository : BaseRepository, IExpenseRepository
    {
        public ExpenseRepository(string connectionString) : base(connectionString)
        {

        }

        public void Delete(int? Id)
        {
            SqlParameter[] parameters = { new SqlParameter("@Id", SqlDbType.Int) { Value = Id } };
            base.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedures.expenses_Delete, parameters);
        }

        private DataTable GetExpensData(List<Expense> expenses)
        {
            
            DataTable dt = new DataTable();
            dt.Columns.Add("ExpenseID");
            dt.Columns.Add("ExpenseDetails");
            dt.Columns.Add("ExpenseAmount");
            foreach(var expense in expenses)
            {
                dt.Rows.Add(expense.Id, expense.Details, expense.Amount);
            }
            
            return dt;
        }

        private string ConverToJSON(List<Expense> expenses)
        {
            string expensesJson = JsonSerializer.Serialize(expenses);

            return expensesJson;
        }
        public void ExpensesUpdate(List<Expense> expenses)
        {
            //SqlParameter[] parameters = { new SqlParameter() {ParameterName = "@TableParam", TypeName = "udtExpenses", SqlDbType = SqlDbType.Structured, Value = GetExpensData(expenses) } };
           
            SqlParameter[] parameters = { new SqlParameter("@MainJson", SqlDbType.NVarChar) { Value = ConverToJSON(expenses) } };

            base.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedures.expenses_ListUpdateJSON, parameters);
        }

        public Expense GetExpenseById(int? Id)
        {
            SqlParameter[] parameters = { new SqlParameter("@Id", SqlDbType.Int) { Value = Id } };

            return base.ConvertDataTableToList<Expense>(base.ExecuteDataTable(CommandType.StoredProcedure, StoredProcedures.expenses_GetById, parameters)).FirstOrDefault();
        }

        public IEnumerable<Expense> GetExpenses()
        {
            return base.ConvertDataTableToList<Expense>(base.ExecuteDataTable(CommandType.StoredProcedure, StoredProcedures.expenses_Get));          
        }

        public void InsertNew(Expense expense)
        {

            SqlParameter[] parameters = {
                                         new SqlParameter("@Details", SqlDbType.NVarChar){Value = expense.Details},
                                          new SqlParameter("@Amount", SqlDbType.Decimal) {Value = expense.Amount}
            };
            base.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedures.expenses_AddNew, parameters);
        }

        public void Update(Expense expense, int? Id)
        {

            SqlParameter[] parameters = { new SqlParameter("@Id", SqlDbType.Int) {Value = Id} ,
                                          new SqlParameter("@Details", SqlDbType.NVarChar) {Value = expense.Details},
                                          new SqlParameter("@Amount", SqlDbType.Decimal) {Value = expense.Amount}
            };

            base.ExecuteNonQuery(CommandType.StoredProcedure, StoredProcedures.expenses_Update, parameters);
        }
    }
}
