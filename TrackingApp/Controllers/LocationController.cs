﻿using System.Collections.Generic;
using Common.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace TrackingApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class LocationController : ControllerBase
    {

        private readonly ILocationService locationService;

        public LocationController(ILocationService _locationService)
        {
            this.locationService = _locationService;
        }

        //GET: api/Location
       [HttpGet]
        public IEnumerable<Location> GetAllLocations()
        {
            return locationService.GetLocations();
        }

        // GET: api/Location/5
        [HttpGet("{id}", Name = "GetLocation")]

            public IActionResult GetById(int id)
            {
            var location = locationService.GetLocationById(id);
                if (location == null)
                {
                    return NotFound();
                }
            //return new ObjectResult(location);
            return Ok(location);
            }

            

        // POST: api/Location
        [HttpPost]
        public void Post([FromBody] Location location)
        {
            locationService.InsertNew(location);
        }

        // PUT: api/Location/5
        [HttpPut("{id}")]
        public void Put([FromBody] Location location, int id)
        {
            locationService.Update(location, id);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            locationService.Delete(id);
        }
    }
}
