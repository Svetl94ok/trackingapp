﻿using System.Collections.Generic;
using Common.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace TrackingApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MeetingController : ControllerBase
    {
        private readonly IMeetingService meetingService;

        public MeetingController(IMeetingService _meetingService)
        {
            this.meetingService = _meetingService;
        }
        // GET: api/Meeting
        [HttpGet]
        public IEnumerable<Meeting> GetAllMeetings()
        {
            return meetingService.GetMeetings();
        }

        // GET: api/Meeting/5
        [HttpGet("{id}", Name = "GetMeeting")]
        public IActionResult Get(int id)
        {
            var meeting = meetingService.GetMeetingById(id);
            if (meeting == null)
            {
                return NotFound();
            }
            return Ok(meeting);
        }

        // POST: api/Meeting
        [HttpPost]
        public void Post([FromBody] Meeting meeting)
        {
            meetingService.InsertNew(meeting);
        }

        // PUT: api/Meeting/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Meeting meeting)
        {
            meetingService.Update(meeting, id);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            meetingService.Delete(id);
        }
    }
}
