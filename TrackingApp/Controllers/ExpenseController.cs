﻿using System.Collections.Generic;
using BLL;
using Common.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace TrackingApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ExpenseController : ControllerBase
    {
        private readonly IExpenseService expenseService;

        public ExpenseController(IExpenseService _expenseService)
        {
            this.expenseService = _expenseService;
        }

        // GET: api/Expense
        [HttpGet]
        public IEnumerable<Expense> GetAllExpenses()
        {
            return expenseService.GetExpenses();
        }

        // GET: api/Expense/5
        [HttpGet("{id}", Name = "GetExpense")]
        public IActionResult Get(int id)
        {
            var expense = expenseService.GetExpenseById(id);
            if (expense == null)
            {
                return NotFound();
            }
            return Ok(expense);
        }

        // POST: api/Expense
        [HttpPost]
        public void Post([FromBody] Expense expense)
        {
            expenseService.InsertNew(expense);
        }

        // PUT: api/Expense/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Expense expense)
        {
            expenseService.Update(expense, id);
        }

        [HttpPut]
        public void Put([FromBody] List <Expense> expenses)
        {
            expenseService.ExpensesUpdate(expenses);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            expenseService.Delete(id);
        }
    }
}
