using BLL;
using Common.Constants;
using Common.Interfaces;
using DAL;
using DAL.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System.IO;
using System.Text;

namespace TrackingApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["Jwt:Issuer"],
                    ValidAudience = Configuration["Jwt:Audience"],
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                };
            });

            services.AddMvc();
            services.AddControllers().AddNewtonsoftJson();

            services.AddSingleton<ILocationRepository>(s => new LocationRepository(DBConst.connectionString));
            services.AddScoped<ILocationService, LocationService>();
            services.AddSingleton<IExpenseRepository>(s => new ExpenseRepository(DBConst.connectionString));
            services.AddScoped<IExpenseService, ExpenseService>();
            services.AddSingleton<IMeetingRepository>(s => new MeetingRepository(DBConst.connectionString));
            services.AddScoped<IMeetingService, MeetingService>();
            services.AddSingleton<IUserRepository>(s => new UserRepository(DBConst.connectionString));
            services.AddScoped<IUserService, UserService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            app.UseStatusCodePages();

            app.Use(async (context, next) =>
            {
                await next();
      
               
                if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value) &&  !context.Request.Path.Value.StartsWith("/api/"))
                {
                    
                    context.Request.Path = "/app/index.html";
                    context.Response.StatusCode = 200;
                    await next();
                }

            });

            app.UseDefaultFiles();

            app.UseStaticFiles();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
