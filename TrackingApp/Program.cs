using Common.Constants;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;


namespace TrackingApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DBCreation.CreateDataBase();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
