﻿angular.module('app')
    .component('expenseGrid',
        {
            bindings: {},
            templateUrl: 'app/expense/expense-grid/expense-grid.component.html',
            controller: ['expenseService', '$window', '$state', '$uibModal', function (expenseService, $window, $state, $uibModal) {


                var self = this;
                self.expenses = [];
                self.editedExpenses = [];
                self.selected = [];
                self.edit = false;

                self.loadExpenses = function () {
                    if (!$window.localStorage.getItem('token')) {
                        self.modalInstance = $uibModal.open({
                            component: 'loginPopup',
                            size: 'lg'
                        }).closed.then(function () {
                            $state.go("login");
                        })
                    }
                    else {
                        expenseService.getExpenses().then(function (res) {
                            self.expenses = res.data;
                        });
                    }
                };

                self.loadExpenses();

                self.query = {
                    order: '',
                    limit: 5,
                    page: 1
                }

                self.sort = {
                    active: '',
                    column: '',
                    descending: false
                };

                self.changeSorting = function (column) {
                    var sort = self.sort;
                    if (sort.column == column) {
                        sort.descending = !sort.descending;
                    }
                    else {
                        sort.column = column;
                        sort.descending = false;
                    }
                };

                self.getIcon = function (column) {

                    var sort = self.sort;

                    if (sort.active == column) {
                        return sort.descending
                            ? 'fas fa-chevron-up'
                            : 'fas fa-chevron-down';
                    }

                    return 'glyphicon-star';
                };

                self.editMode = function () {
                    self.edit = !self.edit;
                }

                self.orderBy = function (column) {

                    if (!self.edit) {
                        self.order.column = column;
                        self.order.reverse = !self.order.reverse; //toggle revers
                        self.data = orderByFilter(self.expenses, self.order.column, self.order.reverse);
                    }
                }

                self.AddEitedExpense = function (expense) {
                    self.editedExpenses.push(expense);
                };

                self.saveAll = function () {
                    expenseService.putExpenses(self.editedExpenses).then(function () {
                        self.editMode();
                        self.loadExpenses();
                    }, function (err) {
                            console.log("Err" + err);
                    })
                };

                self.delete = function (expense) {
                    expenseService.delete(expense.id).then(function () {
                        self.message = "Deleted Successfuly";
                        self.loadExpenses();
                    }, function (err) {
                        console.log("Err" + err);
                    });
                };
               
            }]

        });

