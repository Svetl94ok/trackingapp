﻿angular.module('app')
    .component('expenseList',
        {
            bindings: {},
            templateUrl: 'app/expense/expense-list/expense-list.component.html',
            controller: ['expenseService', '$uibModal', '$window', '$state', function (expenseService, $uibModal, $window, $state) {


                var self = this;
                self.expenses = [];
                self.selectedExpense = null;
               
                self.loadExpenses = function () {
                    if (!$window.localStorage.getItem('token')) {
                        self.modalInstance = $uibModal.open({
                            component: 'loginPopup',
                            size: 'lg'
                        }).closed.then(function () {
                            $state.go("login");
                        })
                    }
                    else {
                        expenseService.getExpenses().then(function (res) {
                            self.expenses = res.data;
                        });
                    }
                };

                self.delete = function (expense) {
                    expenseService.delete(expense.id).then(function () {
                        self.message = "Deleted Successfuly";
                        self.loadExpenses();
                    }, function (err) {
                        console.log("Err" + err);
                    });
                }

                self.loadExpenses();

                self.edit = function (expense) {
                    self.modalInstance = $uibModal.open({
                        component: 'expensePopup',
                        size: 'sm',
                        resolve: {
                            expense: function () {
                                return expense;
                            }
                        }
                    }).closed.then(function () {
                        self.loadExpenses();
                    });
                };


            }]

        });

