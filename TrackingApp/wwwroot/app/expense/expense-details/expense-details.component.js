﻿angular.module('app')
    .component('expenseDetails',
        {
            binding: {
            },
            templateUrl: 'app/expense/expense-details/expense-details.component.html',
            controller: ['expenseService', '$stateParams', '$state', function (expenseService, $stateParams, $state) {
                var self = this;
                self.expense = null;

                self.$onInit = function () {
                    if ($stateParams.id) {
                        expenseService.get($stateParams.id).then(function (res) {
                            self.expense = res.data;
                        })
                    }
                    else {
                        self.expense = {};
                    }
                };

                self.save = function () {
                    var promise;
                    if (self.expense.id) {
                        promise = expenseService.put(self.expense.id, self.expense).then(function () {
                            self.message = "Updated Successfuly";
                        }, function (err) {
                            console.log("Err" + err);
                        });
                    }
                    else {
                        promise = expenseService.post(self.expense).then(function () {
                            self.message = "Created Successfuly";
                        }, function (err) {
                            console.log("Err" + err);
                        });
                    }
                    promise.then(function () {
                        $state.go("expense");
                    })
                }
            }]

        });