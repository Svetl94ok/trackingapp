﻿angular.module('app')
    .component('expensePopup',
        {
            bindings: {
                resolve: "<",
                close: "&",
                dismiss: "&"
            },
            templateUrl: 'app/expense/expense-popup/expense-popup.component.html',
            controller: ['expenseService', function (expenseService) {
                var self = this;
                self.expense = {};


                self.$onInit = function () {
                    if (self.resolve.expense.id) {
                        expenseService.get(self.resolve.expense.id).then(function (res) {
                            self.expense = res.data;
                        })
                    }
                }

                self.save = function () { 
                    if (self.expense.id) {
                       expenseService.put(self.expense.id, self.expense).then(function () {
                           
                        }, function (err) {
                            console.log("Err" + err);
                        })
                        self.close({ $value: self.expense })
                    }             
                };

               

                self.cancel = function () {
                    self.dismiss({ $value: 'cancel' })
                };

            

            }]

        });