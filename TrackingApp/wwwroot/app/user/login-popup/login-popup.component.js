﻿angular.module('app')
    .component('loginPopup',
        {
            bindings: {
                resolve: "<",
                close: "&",
                dismiss: "&"
            },
            templateUrl: 'app/user/login-popup/login-popup.component.html',
            controller: ['$state', function ($state) {
                var self = this;

                self.ok = function () {
                    self.close({ $value: 'close' });
                    }
                
                //self.cancel = function () {
                //    self.dismiss({ $value: 'cancel' })
                //}

            }]

        });