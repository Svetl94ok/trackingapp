﻿angular.module('app')
    .component('userLogin',
        {
            bindings: {
            },
            templateUrl: 'app/user/user-login/user-login.component.html',
            controller: ['AuthenticationService', '$location', function (AuthenticationService, $location) {
                var self = this;

                initController();

                function initController() {
                    // reset login status
                    AuthenticationService.Logout();
                };

                self.login = function login(user) {
                    self.loading = true;
                    AuthenticationService.Login(user.email, user.password, function (result) {
                        if (result === true) {
                            $location.path('/');
                        } else {
                            self.error = 'Username or password is incorrect';
                            self.loading = false;
                        }
                    })
                };

            }]
                });