﻿app.factory('authInterceptor', function ($window, $q, $state) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($window.localStorage.getItem('token')) {
             
                config.headers.Authorization = 'Bearer ' + $window.localStorage.getItem('token');
            }
            return config || $q.when(config);
        },
        response: function (response) {
            if (response.status === 401) {
                //  Redirect user to login page / signup Page.
                $state.go("home");
            }
            return response || $q.when(response);
        }
    };
});


