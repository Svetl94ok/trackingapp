﻿app.constant('API_URL', 'https://localhost:44303/api/');
angular.module('app')
    .config(function ($urlRouterProvider, $locationProvider, $stateProvider, $mdThemingProvider, $httpProvider) {
        $httpProvider.interceptors.push('authInterceptor');

        $mdThemingProvider.theme('primary').primaryPalette('blue').accentPalette('orange');

        $stateProvider
            .state('login', {
                url: '/login',
                template: '<user-login></user-login>'
            })
            .state('home', {
                url: '/',
                template: '<h1>Hello</h1>'
            })
            .state('meeting', {
                url: '/meeting',
                template: '<meeting-list></meeting-list>'
            })
            .state('meeting-new', {
                url: '/meeting/new',
                template: '<meeting-details></meeting-details>'
            })
            //.state('meeting-details', {
            //    url: '/meeting/{id:int}',
            //    template: '<meeting-details></meeting-details>'
            //})
            .state('location', {
                url: '/location',
                template: '<location-list></location-list>'
            })
            .state('location-new', {
                url: '/location/new',
                template: '<location-details></location-details>'
            })
            //.state('location-details', {
            //    url: '/location/{id:int}',
            //    template: '<location-details></location-details>'
            //})
            //.state('expense', {
            //    url: '/expense',
            //    template: '<expense-list></expense-list>'
            //})
            .state('expense', {
                url: '/expense',
                template: '<expense-grid></expense-grid>'
            })
            //.state('expense-details', {
            //    url: '/expense/{id:int}',
            //    template: '<expense-details></expense-details>'
            //})
            .state('expense-new', {
                url: '/expense/new',
                template: '<expense-details></expense-details>'
            });


        $urlRouterProvider.otherwise("/");
        $locationProvider.html5Mode(true);
    });
                