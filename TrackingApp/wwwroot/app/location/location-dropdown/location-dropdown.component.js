﻿angular.module('app')
    .component('locationDropdown',
        {
            bindings: {
                locations: '='
            },
            templateUrl: 'app/location/location-dropdown/location-dropdown.component.html',
            controller: ['locationService', function (locationService) {
                var self = this;
                self.locations = [];

                locationService.getLocations().then(function (res) {
                    self.locations = res.data;
                });

            }]
        });