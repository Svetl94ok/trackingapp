﻿angular.module('app')
    .component('locationList',
        {
            bindings: {},
            templateUrl: 'app/location/location-list/location-list.component.html',
            controller: ['locationService', '$uibModal', '$window', '$state', function (locationService, $uibModal, $window, $state) {
                var self = this;
                self.locations = [];
                self.selectedLocation = null;

                self.loadLocations = function () {
                    if (!$window.localStorage.getItem('token')) {
                            self.modalInstance = $uibModal.open({
                                component: 'loginPopup',
                                size: 'lg'
                            }).closed.then(function () {
                                $state.go("login");
                            });                      
                    }
                    else {
                        locationService.getLocations().then(function (res) {
                            self.locations = res.data;
                        });
                    }
                };

                self.edit = function (location) {
                    self.modalInstance = $uibModal.open({
                        component: 'locationPopup',
                        size: 'sm',
                        resolve: {
                            location: function () {
                                return location;
                            }
                        }
                    }).closed.then(function () {
                        self.loadLocations();
                    });
                };
                
                self.delete = function (location) {
                    locationService.delete(location.id).then(function () {
                        self.message = "Deleted Successfuly";
                        self.loadLocations();
                    }, function (err) {
                        console.log("Err" + err);
                    });
                }

                self.loadLocations();
                
            }]
        
        });