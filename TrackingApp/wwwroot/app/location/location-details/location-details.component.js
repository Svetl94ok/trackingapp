﻿angular.module('app')
    .component('locationDetails',
        {
            bindings: {},
            templateUrl: 'app/location/location-details/location-details.component.html',
            controller: ['locationService', '$stateParams', '$state', function (locationService, $stateParams, $state) {
                var self = this;
                self.location = null;

                self.$onInit = function () {
                    if ($stateParams.id) {
                        locationService.get($stateParams.id).then(function (res) {
                            self.location = res.data;
                        })
                    }
                    else {
                        self.location = {};
                    }
                };

                self.save = function () {
                    var promise;
                    if (self.location.id) {
                        promise = locationService.put(self.location.id, self.location).then(function () {
                            self.message = "Updated Successfuly";
                        }, function (err) {
                            console.log("Err" + err);
                        });
                    }
                    else {                       
                        promise = locationService.post(self.location).then(function () {
                            self.message = "Created Successfuly";
                        }, function (err) {
                            console.log("Err" + err);
                        });
                    }
                    promise.then(function () {
                        $state.go("location");
                    })
                }
            }]

        });