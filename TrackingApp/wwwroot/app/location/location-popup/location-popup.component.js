﻿angular.module('app')
    .component('locationPopup',
        {
            bindings: {
                resolve: "<",
                close: "&",
                dismiss: "&"
            },
            templateUrl: 'app/location/location-popup/location-popup.component.html',
            controller: ['locationService', function (locationService) {
                var self = this;
                self.location = {};


                self.$onInit = function () {
                    if (self.resolve.location.id) {
                        locationService.get(self.resolve.location.id).then(function (res) {
                            self.location = res.data;
                        })
                    }
                }

                self.save = function () {
                    if (self.location.id) {
                        locationService.put(self.location.id, self.location).then(function () {

                        }, function (err) {
                            console.log("Err" + err);
                        })
                        self.close({ $value: self.location })
                    }
                };



                self.cancel = function () {
                    self.dismiss({ $value: 'cancel' })
                };



            }]

        });