﻿angular.module('app')
    .component('navigation',
        {
            bindings: {},
            templateUrl: 'app/navigation/navigation.component.html',
            controller: ['AuthenticationService', function (AuthenticationService) {

                var self = this;
                self.Logout = function() {
                    AuthenticationService.Logout();
                    alert("You have been logged out")
                }
            }]
        });