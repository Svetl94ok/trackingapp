﻿app.factory('AuthenticationService', Service);

function Service($http, $window, $localStorage) {
    var service = {};

    service.Login = Login;
    service.Logout = Logout;

    return service;

    function Login(email, password, callback) {
        $http.post('/api/login', { email: email, password: password })
            .then(function (response) {
                // login successful if there's a token in the response
                if (response.data) {

                   
                    // store username and token in local storage to keep user logged in between page refreshes
                    $localStorage.currentUser = { email: email, token: response.data };                  
              

                    // add jwt token to auth header for all requests made by the $http service
                    $http.defaults.headers.common.Authorization = 'Bearer ' + response.data;


                    $window.localStorage.setItem('token', response.data);

                    // execute callback with true to indicate successful login
                    callback(true);
                } else {
                    // execute callback with false to indicate failed login
                    callback(false);
                }
            });
    }

    function Logout() {
        // remove user from local storage and clear http auth header
        delete $localStorage.currentUser;
        window.localStorage.removeItem('token');
        $http.defaults.headers.common.Authorization = '';
    }
}
