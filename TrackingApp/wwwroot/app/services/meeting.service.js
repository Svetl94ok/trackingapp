﻿app.service('meetingService', function ($http) {
    //Create new record
    this.post = function (Meeting) {
        var request = $http({
            method: "post",
            url: "/api/Meeting",
            data: Meeting
        });
        return request;
    }
    //Get Single Records
    this.get = function (id) {
        return $http.get("/api/Meeting/" + id);
    }

    //Get All Meetings
    this.getMeetings = function () {
        return $http.get("/api/Meeting");
    }


    //Update the Record
    this.put = function (id, Meeting) {
        var request = $http({
            method: "put",
            url: "/api/Meeting/" + id,
            data: Meeting
        });
        return request;
    }
    //Delete the Record
    this.delete = function (id) {
        var request = $http({
            method: "delete",
            url: "/api/Meeting/" + id
        });
        return request;
    }
});
