﻿app.service('expenseService', function ($http) {
 
    //Create new record
    this.post = function (Expense) {
        var request = $http({
            method: "post",
            url: "/api/Expense",
            data: Expense
        });
        return request;
    }
    //Get Single Records
    this.get = function (id) {
        return $http.get("/api/Expense/" + id);
    }

    //Get All Expenses
    this.getExpenses = function () {
        return $http.get("/api/Expense");
    }

    //Update the Record
    this.put = function (id, Expense) {
        var request = $http({
            method: "put",
            url: "/api/Expense/" + id,
            data: Expense
        });
        return request;
    }

    //Update the Record
    this.putExpenses = function (expenses) {
        var request = $http({
            method: "put",
            url: "/api/Expense",
            accept: "application/json",
            data: expenses
        });
        return request;
    }
    //Delete the Record
    this.delete = function (id) {
        var request = $http({
            method: "delete",
            url: "/api/Expense/" + id
        });
        return request;
    }
});
