﻿app.service('locationService', function ($http) {

    //Create new record
    this.post = function (Location) {
        var request = $http({
            method: "post",
            url: "/api/Location",
            data: Location
        });
        return request;
    }
    //Get Single Records
    this.get = function (id) {
        return $http.get("/api/Location/" + id);
    }

    //Get All Locations
    this.getLocations = function () {
        return $http.get("/api/Location");
    }


    //Update the Record
    this.put = function (id, Location) {
        var request = $http({
            method: "put",
            url: "/api/Location/" + id,
            data: Location
        });
        return request;
    }
    //Delete the Record
    this.delete = function (id) {
        var request = $http({
            method: "delete",
            url: "/api/Location/" + id
        });
        return request;
    }
});
