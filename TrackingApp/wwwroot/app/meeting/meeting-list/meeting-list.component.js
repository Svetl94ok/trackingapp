﻿
angular.module('app')
    .component('meetingList',
        {
            bindings: {},
            templateUrl: 'app/meeting/meeting-list/meeting-list.component.html',
            controller: ['meetingService', '$uibModal', '$window', '$state', function (meetingService, $uibModal, $window, $state) {
                var self = this;
              
                self.meetings = [];
                self.selectedMeeting = null;
               

                self.loadMeetings = function () {
                    if (!$window.localStorage.getItem('token')) {
                        self.modalInstance = $uibModal.open({
                            component: 'loginPopup',
                            size: 'lg'
                        }).closed.then(function () {
                            $state.go("login");
                        });
                    }
                    else {
                        meetingService.getMeetings().then(function (res) {
                            self.meetings = res.data;
                        });
                    }
                };
                

                self.delete = function (meeting) {
                   meetingService.delete(meeting.id).then(function () {
                        self.message = "Deleted Successfuly";
                        self.loadMeetings();
                    }, function (err) {
                        console.log("Err" + err);
                    });
                }

                self.loadMeetings();


                self.edit = function (meeting) {
                    self.modalInstance = $uibModal.open({
                        component: 'meetingPopup',
                        size: 'lg',
                        resolve: {
                            meeting: function () {
                                return meeting;
                            }
                        }
                    }).closed.then(function () {
                        self.loadMeetings();
                    });
                };

            }]

        });