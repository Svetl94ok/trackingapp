﻿angular.module('app')
    .component('meetingDetails',
        {
            bindings: {},
            templateUrl: 'app/meeting/meeting-details/meeting-details.component.html',
            controller: ['meetingService', 'locationService', '$stateParams', '$state', function (meetingService, locationService, $stateParams, $state) {
                var self = this;
                self.meeting = null;


                self.$onInit = function () {
                    if ($stateParams.id) {
                        meetingService.get($stateParams.id).then(function (res) {
                            self.meeting = res.data;                                            
                        })
                    }
                    else {
                        self.meeting = {};
                    }
                };


                self.locations = [];

                locationService.getLocations().then(function (res) {
                    self.locations = res.data;
                });
                self.save = function () {
                    var promise;
                    if (self.meeting.id) {
                        promise = meetingService.put(self.meeting.id, self.meeting).then(function () {
                            self.message = "Updated Successfuly";
                        }, function (err) {
                            console.log("Err" + err);
                        });
                    }
                    else {
                        promise = meetingService.post(self.meeting).then(function () {
                            self.message = "Created Successfuly";
                        }, function (err) {
                            console.log("Err" + err);
                        });
                    }
                    promise.then(function () {
                        $state.go("meeting");
                    })
                }
            }]

        });