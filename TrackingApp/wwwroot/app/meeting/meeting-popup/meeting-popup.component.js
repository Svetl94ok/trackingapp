﻿angular.module('app')
    .component('meetingPopup',
        {
            bindings: {
                resolve: "<",
                close: "&",
                dismiss: "&"
            },
            templateUrl: 'app/meeting/meeting-popup/meeting-popup.component.html',
            controller: ['meetingService', 'locationService', function (meetingService, locationService) {
                var self = this;
                self.meeting = {};


                self.$onInit = function () {
                    if (self.resolve.meeting.id) {
                        meetingService.get(self.resolve.meeting.id).then(function (res) {
                            self.meeting = res.data;
                        })
                    }
                }

                self.locations = [];

                locationService.getLocations().then(function (res) {
                    self.locations = res.data;
                });

                self.save = function () {
                    if (self.meeting.id) {
                        meetingService.put(self.meeting.id, self.meeting).then(function () {

                        }, function (err) {
                            console.log("Err" + err);
                        })
                        self.close({ $value: self.meeting })
                    }
                };

                self.cancel = function () {
                    self.dismiss({ $value: 'cancel' })
                };

            }]

        });