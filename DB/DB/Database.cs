﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DB.DB
{
    public class Database
    {
        public static string sqlDataSource = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public DataTable GetData(string str)
        {
            DataTable objresutl = new DataTable();
            try
            {
                SqlDataReader myReader;

                using (SqlConnection connection = new SqlConnection(sqlDataSource))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(str, connection))
                    {
                        myReader = command.ExecuteReader();
                        objresutl.Load(myReader);

                        myReader.Close();
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return objresutl;

        }
        public int ExecuteData(string str, params IDataParameter[] sqlParams)
        {
            int rows = -1;
            try
            {

                using (SqlConnection connection = new SqlConnection(sqlDataSource))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(str, connection))
                    {
                        if (sqlParams != null)
                        {
                            foreach (IDataParameter par in sqlParams)
                            {
                                cmd.Parameters.Add(par);
                            }
                            rows = cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }


            return rows;


        }
    }
}
