﻿using DB.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DB.Repositories
{
    class LocationRepository : ILocationRepository
    {
        private readonly string connection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public void Delete(Location location)
        {
            using(SqlConnection con = new SqlConnection(connection))
            {
                var command = new SqlCommand("spDeleteLocation", con);
                command.CommandType = CommandType.StoredProcedure;
                con.Open();
                command.Parameters.AddWithValue("@Id", location.Id);
                command.ExecuteNonQuery();
            }
        }

        public Location GetLocationById(int? id)
        {
            Location location = new Location();
            using (SqlConnection con = new SqlConnection(connection))
            {
                SqlCommand command = new SqlCommand("spGetLocation", con);
                command.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    location.Id = Convert.ToInt32(dataReader["Id"]);
                    location.Name = dataReader["Name"].ToString();
                    location.Address = dataReader["Address"].ToString();
                }
                return location;
            }
        }

        public IList<Location> GetLocations()
        {
            IList<Location> locations = new List<Location>();
            using(SqlConnection con = new SqlConnection(connection))
            {
                SqlCommand command = new SqlCommand("spGetLocation", con);
                command.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    var location = new Location()
                    {
                        Id = Convert.ToInt32(dataReader["Id"]),
                        Name = dataReader["Name"].ToString(),
                        Address = dataReader["Address"].ToString()
                    };
                    locations.Add(location);
                }
                return (locations);
            }
        }

        public void InsertNew(Location location)
        {
            throw new NotImplementedException();
        }

        public void Update(Location location)
        {
            using(SqlConnection con = new SqlConnection(connection))
            {
                var command = new SqlCommand("spAddNewLocation", con);
                con.Open();
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Name", location.Name);
                command.Parameters.AddWithValue("@Address", location.Address);
            }
        }
    }
}
