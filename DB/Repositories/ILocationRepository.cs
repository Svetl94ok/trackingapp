﻿using DB.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DB.Repositories
{
    public interface ILocationRepository
    {
        IList<Location> GetLocations();
        Location GetLocationById(int? id);
        void InsertNew(Location location);
        void Update(Location location);
        void Delete(Location location);
    }
}
