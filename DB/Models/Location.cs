﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DB.Models
{
    [Table("Locations")]
    public class Location
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Name is required field")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Address is required field")]
        public string Address { get; set; }

    }
}
