﻿using Common.Interfaces;
using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    public class LocationService : ILocationService
    {
        private readonly ILocationRepository locationRepository;

        public LocationService(ILocationRepository _locationRepository)
        {
            locationRepository = _locationRepository;
        }
        public void Delete(int? id)
        {
            locationRepository.Delete(id);
        }

        public Location GetLocationById(int? id)
        {
            return locationRepository.GetLocationById(id);
        }

        public IEnumerable<Location> GetLocations()
        {
            return locationRepository.GetLocations();
        }

        public void InsertNew(Location location)
        {
            locationRepository.InsertNew(location);
        }

        public void Update(Location location, int? id)
        {
            locationRepository.Update(location, id);
        }
    }
}
