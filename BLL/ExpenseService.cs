﻿using Common.Interfaces;
using Models;
using System.Collections.Generic;

namespace BLL
{
    public class ExpenseService : IExpenseService
    {
       private readonly IExpenseRepository expenseRepository;

        public ExpenseService(IExpenseRepository _expenseRepository)
        {
            this.expenseRepository = _expenseRepository;
        }

        public void Delete(int? id)
        {
            expenseRepository.Delete(id);
        }

        public Expense GetExpenseById(int? id)
        {
            return expenseRepository.GetExpenseById(id);
        }

        public IEnumerable<Expense> GetExpenses()
        {
            return expenseRepository.GetExpenses();
        }
        public void InsertNew(Expense expense)
        {
            expenseRepository.InsertNew(expense);
        }

        public void Update(Expense expense, int? id)
        {
            expenseRepository.Update(expense, id);
        }

        public void ExpensesUpdate(List<Expense> expenses)
        {
            expenseRepository.ExpensesUpdate(expenses);
        }
    }
}
