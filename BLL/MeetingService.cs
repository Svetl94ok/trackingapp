﻿using Common.Interfaces;
using Models;
using System.Collections.Generic;
using System.Linq;

namespace BLL
{
    public class MeetingService : IMeetingService
    {
        private readonly IMeetingRepository meetingRepository;
        private readonly ILocationService locationService;

        public MeetingService(IMeetingRepository _meetingRepository, ILocationService _locationService)
        {
            meetingRepository = _meetingRepository;
            this.locationService = _locationService;
        }

        public void Delete(int? id)
        {
            meetingRepository.Delete(id);
        }

        public Meeting GetMeetingById(int? id)
        {
            return meetingRepository.GetMeetingById(id);
        }

        public IEnumerable<Meeting> GetMeetings()
        {
            var locations = locationService.GetLocations();
            return meetingRepository.GetMeetings().Select(x => 
            {
                x.Location = locations.FirstOrDefault(l => l.Id == x.Location_ID);
                return x;
            });
        }

        public void InsertNew(Meeting meeting)
        {
            meetingRepository.InsertNew(meeting);
        }

        public void Update(Meeting meeting, int? id)
        {
            meetingRepository.Update(meeting, id);
        }
    }
}
