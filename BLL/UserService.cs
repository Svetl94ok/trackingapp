﻿using Common.Interfaces;
using Models;
using System.Collections.Generic;


namespace BLL
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;

        public UserService(IUserRepository _userRepository)
        {
            this.userRepository = _userRepository;
        }
        public void Delete(int? id)
        {
            userRepository.Delete(id);
        }

        public User GetUsergById(int? id)
        {
            return userRepository.GetUserById(id);
        }

        public IEnumerable<User> GetUsers()
        {
            return userRepository.GetUsers();
        }

        public void InsertNew(User user)
        {
            userRepository.InsertNew(user);
        }

        public void Update(User user, int? id)
        {
            userRepository.Update(user, id);
        }
    }
}
